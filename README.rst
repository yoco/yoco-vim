Install This `vimrc`
--------------------

Open terminal::

  git clone --recursive https://bitbucket.org/yoco/yoco-vim.git ~/.vim
  ln -fs ~/.vim/.vimrc ~/.vimrc

Run Vundle Install::

  vi
  :BundleInstall<CR>
  :q<CR>

Install YouCompleteMe
---------------------

Ubuntu has package ``vim-youcompleteme`` since 14.04.
So I don't have to build YCM by myself, just ``apt-get`` it::

  sudo apt-get install vim-youcompleteme
  ln -s /usr/share/vim-youcompleteme ~/.vim/bundle/YouCompleteMe

Then enable YouCompleteMe bundle in .vimrc, change::

  "Plugin 'Valloric/YouCompleteMe'

to::

  Plugin 'Valloric/YouCompleteMe'

save and restart vim.

Or, if I need newer version YCM, I have to build YCM by myself. see
https://github.com/Valloric/YouCompleteMe for detail.

Newer YCM also need newer vim. go http://www.vim.org get and build it.
