" ======================================
" Vundle
" ======================================
set nocompatible  " iMproved
"filetype off      " required!

set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" let Vundle manage Vundle
" required! 
Plugin 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
"Plugin 'mileszs/ack.vim'
"Plugin 'fcamel/gj'
"Plugin 'tpope/vim-fugitive'
"Plugin 'Lokaltog/vim-easymotion'
Plugin 'easymotion/vim-easymotion'
Plugin 'godlygeek/tabular'
Plugin 'scrooloose/nerdcommenter'
"Plugin 'scrooloose/syntastic'
"Plugin 'airblade/vim-gitgutter'
"Plugin 'Valloric/YouCompleteMe'
"Plugin 'nathanaelkane/vim-indent-guides'
" vim-scripts repos
"Plugin 'L9'
"Plugin 'FuzzyFinder'
" non github repos
"Plugin 'git://git.wincent.com/command-t.git'
" ...

call vundle#end()
filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..

" ======================================
" brace
" ======================================

"function! LeftBrace()
"    if match(getline('.'), "\\C\\<struct\\>\\|\\<class\\>\\|\\<enum\\>\\|\\<union\\>") != -1 || match(getline(line('.')-1), "\\C\\<struct\\>\\|\\<class\\>\\|\\<enum\\>\\|\\<union\\>") != -1
"        return "{\<CR> \<BS>\<CR>} ;\<ESC>\<UP>A"
"    else
"        return "{\<CR> \<BS>\<CR>}\<ESC>\<UP>A"
"endfunction

" ======================================
" display
" ======================================
syntax on
"set t_Co=256
"colorscheme ir_black
"set showmatch
"set ruler
"set relativenumber
"set cursorline
"set cursorcolumn
"au GUIEnter * simalt ~x
"set laststatus=2
set splitright
set splitbelow
set nowrap
set hlsearch                            
"set mps+=<:>
"set guioptions-=T
"set guioptions-=m
"set completeopt="longest,menu"
set completeopt="menu"
"set gfn=Bitstream\ Vera\ Sans\ Mono\ 9
"let g:load_doxygen_syntax=1
"let g:doxygen_enhanced_color=0

" ======================================
" my key map
" ======================================
"nnoremap h <NOP>
"nnoremap j <NOP>
"nnoremap k <NOP>
"nnoremap l <NOP>

"nnoremap ,s :w!<CR>
"inoremap ,s <ESC>:w!<CR>
"inoremap <C-S> <SPACE><BS><ESC>:w!<CR>a
"inoremap <C-V> <SPACE><BS><ESC>pa
"inoremap jj <ESC>
"inoremap ;jj ;<ESC>
"inoremap zz size_t 
"inoremap tn<SPACE> typename 
"inoremap tl<SPACE> template 
" easymotion
"nmap WW \w
"imap WW <ESC>\w
"nmap BB \b
"imap BB <ESC>\b

nmap ,c \cl
nmap ,u \cu 
let g:NERDSpaceDelims = 1

"autocmd filetype cpp,thorscript inoremap <expr> {{ LeftBrace()
map <silent> <C-PageUp> :bprev<CR>
map <silent> <C-PageDown> :bnext<CR>
"map <silent> <C-H> :set invhlsearch<CR>
map <silent> <C-N> :cn<CR>
map <silent> <C-P> :cp<CR>
map <C-Q> <ESC>
"noremap Q g<C-]>
"noremap T <C-T>
map <C-J> viw"ay:echo "Word yanked"<CR>
map ,j viw"ay:echo "Word yanked"<CR>
map ,d yyP,cj
map <C-K> viw"ap
map <C-L> nviw"ap
map <M-Up> <C-W><Up>
map <M-Down> <C-W><Down>
map <M-Left> <C-W><Left>
map <M-Right> <C-W><Right>
map <C-Left> 4zh
map <C-Right> 4zl
map ,w <C-W>
"map ,D :call InsertDoc()<CR>
map ,v <C-V>
"map ,* :grep --exclude=tags --exclude-dir=.git --exclude-dir=doc '\<<C-R><C-W>\>' . -R<CR>
"map ,8 :let @/='\<<C-R><C-W>\>'<CR>
"map ,y :call StrToChars()<CR>

" ======================================
" my command
" ======================================
"command! CO !cp % %.co; co %
"command! COL !cp % %.co; co %; ssh yoco_xiao@ids95tw co -l teco/build/teco/%
"command! Q q
"command! Qa qa

" ======================================
" vim behavior settings
" ======================================
set nocp
set backspace=2
set ignorecase
set smarttab
"set smartindent
set autoindent
set expandtab
set tabstop=2
set shiftwidth=2
set incsearch
"set foldmethod=marker
"set wildmenu
"set wildmode=list:longest
"set wildignore=*.o,*.obj,*.bak,*.exe
set nrformats=hex
set undodir=~/.vim/undodir
"set undofile
"set undolevels=1000 "maximum number of changes tha can be undone
"set undoreload=10000 "maximum number lines to save for undo on a buffer reload

"set path+=/usr/local/include
"set path+=/c/projects/lychee/src
"set path+=/c/projects/lychee/src/cloudmosa

set makeprg=make

" ======================================
" indent_guides
" ======================================

"let g:indent_guides_indent_levels = 10 " more than 10 is bad XD
"let g:indent_guides_guide_size = 2
"let g:indent_guides_auto_colors = 0
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#303030 ctermbg=233
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#282828 ctermbg=234
"autocmd VimEnter,Colorscheme * :hi Normal ctermbg=232
"autocmd BufReadPost *.cpp IndentGuidesEnable
"autocmd BufReadPost *.cc IndentGuidesEnable
"autocmd BufReadPost *.h IndentGuidesEnable
"autocmd BufReadPost *.t IndentGuidesEnable
""let g:indent_guides_enable_on_vim_startup = 1
"autocmd WinEnter * :set cursorline cursorcolumn
"autocmd WinLeave * :set nocursorline nocursorcolumn

" ======================================
" tags
" ======================================
"set tags+=/work/zp/platform/projects/tags
"set tags+=~/tags/boost/boost_tags
"set tags+=~/tags/websocketpp_tags
"set tags+=~/tags/libc++_tags
"set tags+=~/package/llvm-3.2.src/tags
"set tags+=~/package/OpenCV-2.4.3/modules/tags
"set tags+=./tags
"map <f8> :!ctags -R .<cr>


" ======================================
" easy motion
" ======================================
"nmap ? \\f
nmap { \\b
nmap } \\w

" ======================================
" FuzzyFinder
" ======================================
"nmap ff :FufCoverageFile<CR>
" ======================================
" YouCompleteMe
" ======================================
"let g:ycm_confirm_extra_conf = 0
"let g:ycm_add_preview_to_completeopt = 1
"let g:ycm_server_keep_logfiles = 1
"let g:ycm_server_log_level = 'debug'
